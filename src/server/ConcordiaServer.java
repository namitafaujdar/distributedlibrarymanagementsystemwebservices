package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import javax.xml.ws.Endpoint;

import constants.ConstantValues;
import serverInterfaceImplementation.ConcordiaLibraryImplementation;

/**
 * 
 * @author Namita Faujdar
 *
 */

public class ConcordiaServer {

	public static void main(String[] args) throws Exception{

		ConcordiaLibraryImplementation conStub = new ConcordiaLibraryImplementation();
		
		Endpoint endpoint = Endpoint.publish("http://localhost:8080/comp", conStub);
		System.out.println("Concordia server started....");

		}


}
