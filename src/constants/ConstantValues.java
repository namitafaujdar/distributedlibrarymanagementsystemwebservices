package constants;

/**
 * 
 * @author Namita Faujdar
 *
 */

public class ConstantValues {

	public static final int CON_SERVER_PORT = 7777;
	public static final int MCG_SERVER_PORT = 6666;
	public static final int MON_SERVER_PORT = 5555;

	public static final String CONCORDIA = "CON";
	public static final String MONTREAL = "MON";
	public static final String MCGILL = "MCG";

	public static final String CON_SERVER_NAME = "Concordia";
	public static final String MON_SERVER_NAME = "Montreal";
	public static final String MCG_SERVER_NAME = "McGill";
}
