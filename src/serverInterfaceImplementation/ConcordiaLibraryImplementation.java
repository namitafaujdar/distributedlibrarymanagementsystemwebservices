package serverInterfaceImplementation;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import constants.ConstantValues;
import serverInterface.LibrariesServer;

@WebService(endpointInterface = "serverInterface.LibrariesServer")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class ConcordiaLibraryImplementation implements LibrariesServer{

	private static HashMap<String, HashMap<String, Object>> dataStore = new HashMap<>();
	private static HashMap<String, ArrayList<String>> userBookMapping = new HashMap<>();
	private static HashMap<String, Queue<String>> waitList = new HashMap<>();
	private static Queue<String> userQueue = new LinkedList<String>();
	private static final String KEY_QUANTITY = "quantity";
	private static final String KEY_NAME = "name";
	private static HashMap<String, Boolean> checkBook = new HashMap<>();
	private static Logger log;

	{
		//item1
		dataStore.put("CON1111", new HashMap<String, Object>());
		dataStore.get("CON1111").put(KEY_NAME, "Java");
		dataStore.get("CON1111").put(KEY_QUANTITY, 2);
		checkBook.put("CON1111", true);
		//item2
		dataStore.put("CON1234", new HashMap<String, Object>());
		dataStore.get("CON1234").put(KEY_NAME, "DS");
		dataStore.get("CON1234").put(KEY_QUANTITY, 2);
		checkBook.put("CON1234", true);
		//item3
		dataStore.put("CON2222", new HashMap<String, Object>());
		dataStore.get("CON2222").put(KEY_NAME, "CN");
		dataStore.get("CON2222").put(KEY_QUANTITY, 4);
		checkBook.put("CON2222", true);
		//item4
		dataStore.put("CON2345", new HashMap<String, Object>());
		dataStore.get("CON2345").put(KEY_NAME, "CA");
		dataStore.get("CON2345").put(KEY_QUANTITY, 3);
		checkBook.put("CON2345", true);
		//item5
		dataStore.put("CON3456", new HashMap<String, Object>());
		dataStore.get("CON3456").put(KEY_NAME, "AI");
		dataStore.get("CON3456").put(KEY_QUANTITY, 2);
		checkBook.put("CON3456", true);
		//item6
		dataStore.put("CON6789", new HashMap<String, Object>());
		dataStore.get("CON6789").put(KEY_NAME, "ML");
		dataStore.get("CON6789").put(KEY_QUANTITY, 8);
		checkBook.put("CON6789", true);
	}

	public ConcordiaLibraryImplementation() {
		super();
		log = Logger.getLogger(ConcordiaLibraryImplementation.class.getName());
		try {
			updateConServerLog(log);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized String addItem(String managerID, String itemID, String itemName, int quantity) {
		log.info("Received request from " + managerID +" to add an item with book id " + itemID + " ,book name "+itemName+
				" & quantity "+ quantity);

		if (dataStore.containsKey(itemID)) {
			while(!checkBook.get(itemID));
			checkBook.put(itemID, false);
			int oldQuantity = (Integer) dataStore.get(itemID).get(KEY_QUANTITY);
			dataStore.get(itemID).put(KEY_QUANTITY, oldQuantity + quantity);
		} else {
			HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(KEY_NAME, itemName);
			data.put(KEY_QUANTITY, quantity);
			dataStore.put(itemID, data);
		}
		checkBook.put(itemID, true);
		if(!(waitList.isEmpty() || waitList == null) && waitList.containsKey(itemID)) {
			String queue= waitList.get(itemID).peek();   
			if(queue != null) {
				bookAutoAssign(queue, itemID);
				return "Item assigned to waitlist user.";
			}
		} 

		return "Item added successfully";
	}

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public synchronized String removeItem(String managerID, String itemID, int quantity) {
		log.info("Received request from " + managerID +" to remove an item with book id " + itemID +
				" & quantity "+ quantity);
		while(!checkBook.get(itemID));
		checkBook.put(itemID, false);
		int oldQuantity = (Integer) dataStore.get(itemID).get(KEY_QUANTITY);
		if(quantity < 0) {
			dataStore.remove(itemID);
			//Remove item to user's book mapping
			if(userBookMapping.containsValue(itemID)) {
				userBookMapping.remove(itemID);
			}
			checkBook.put(itemID, true);
			return "Item removed successfully";
		} else if(oldQuantity < quantity){
			checkBook.put(itemID, true);
			return "The quantity you entered is incorrect";
		}else {
			dataStore.get(itemID).put(KEY_QUANTITY, oldQuantity - quantity);
			checkBook.put(itemID, true);
			return "Item quantity decreased successfully";
		}
	}

	@Override
	public synchronized String listItemAvailability(String managerID) {
		log.info("Received request from " + managerID +" to list item");
		String itemId = null;
		String strings = "";
		ArrayList<Object> list = new ArrayList<Object>();
		for(Entry<String, HashMap<String, Object>> key: dataStore.entrySet()) {
			itemId = key.getKey();
			HashMap<String, Object> values = key.getValue();
			for(Entry<String, Object> subKey : values.entrySet()) {
				list.add(subKey.getValue());
			}
			strings += " "+itemId.concat(" ").concat(list.toString());
			list = new ArrayList<Object>();
		}
		return strings;
	}

	@Override
	public synchronized String waitingQueueList(String userId, String itemId) {
		LibrariesServer ls;
		if(itemId.substring(0, 3).equals("MCG")) {
			ls = requestToOtherServers(userId,itemId,4, null, ConstantValues.MCG_SERVER_PORT, null);
			return ls.waitingQueueList(userId, itemId);
		}else if(itemId.substring(0, 3).equals("MON")) {
			ls = requestToOtherServers(userId,itemId,4, null, ConstantValues.MON_SERVER_PORT, null);
			return ls.waitingQueueList(userId, itemId);
		}

		log.info("Received request from " + userId +" to add in waitlist against book id "+itemId);
		if(((userQueue.isEmpty() || userQueue == null) && (waitList == null || waitList.isEmpty())) ||
				!waitList.get(itemId).contains(userId)) {
			userQueue.add(userId);
			waitList.put(itemId, userQueue);
			return "You are added to waiting list.";
		}else if(waitList.containsKey(itemId)){
			Queue<String> queue = waitList.get(itemId);
			for(int i=0; i < queue.size(); i++) {
				if(queue.contains(userId)) {
					break;
				}
			}
			return "You are already in waiting list.";
		}
		return "Something went wrong";
	}

	@Override
	public synchronized String borrowItem(String userID, String itemID) {
		log.info("Received request from " + userID +" for borrowing a book with book id "+itemID);

		if(!userID.substring(0,3).equals(ConstantValues.CONCORDIA) && 
				(userBookMapping != null || !userBookMapping.isEmpty()) && userBookMapping.containsKey(userID)) {

			ArrayList<String> name = userBookMapping.get(userID);
			for (String bookItemId : name) {
				if(bookItemId!=null && ConstantValues.CONCORDIA.contains(bookItemId.substring(0, 3))) {
					return "Sorry! You can borrow only one item.";
				}
			}
		}

		if(itemID.substring(0, 3).equals(ConstantValues.CONCORDIA)) {
			if(!dataStore.containsKey(itemID)) return "Book doesn't exist";
			while(!checkBook.get(itemID));
			checkBook.put(itemID, false);

			int quantity = (Integer) dataStore.get(itemID).get(KEY_QUANTITY);
			if (quantity > 0 && dataStore.containsKey(itemID)) {
				quantity--;

				// Update Quantity
				dataStore.get(itemID).put(KEY_QUANTITY, quantity);

				// Add item to user's book id mapping
				if(userBookMapping == null || userBookMapping.isEmpty() || !userBookMapping.containsKey(userID)) {
					ArrayList<String> bookIds = new ArrayList<String>();
					bookIds.add(itemID);
					userBookMapping.put(userID, bookIds);
				}else if(userBookMapping.get(userID).contains(itemID)){
					checkBook.put(itemID, true);
					return "You have already borrowed this book"; 
				}else if (userBookMapping.containsKey(userID)) {
					userBookMapping.get(userID).add(itemID);
				} 

				checkBook.put(itemID, true);
				return "Book issued!";
			} else if(quantity == 0) {
				if((userBookMapping != null || !userBookMapping.isEmpty()) && userBookMapping.containsKey(userID) &&
						userBookMapping.get(userID).contains(itemID)) {
					checkBook.put(itemID, true);
					return "You have already borrowed this book"; 
				}else {
					checkBook.put(itemID, true);
					return "Book not available!"+"\n"+
					"Do you want to lent the book once available? Press 1 for yes, 2 for no: ";
				}
			}else {
				checkBook.put(itemID, true);
				return "Book doesn't exist";
			}
		}else {
			LibrariesServer ls = requestToOtherServers(userID, itemID, 2,null, 0, null);
			return ls.borrowItem(userID, itemID);
		}
	}

	@Override
	public synchronized String findItem(String userID, String itemName, boolean flag) { 
		log.info("Received request from " + userID +" for finding a book with book name "+itemName);

		LibrariesServer ls;
		String valueString = null ;
		Integer quantity = null;
		String itemId = null;
		String resultString = "";
		for (String key : dataStore.keySet()) {
			itemId = key;
			valueString = (String) dataStore.get(key).get(KEY_NAME); 
			quantity = (Integer)dataStore.get(key).get(KEY_QUANTITY);
			if (itemName.equalsIgnoreCase(valueString)) {
				resultString += itemId.concat(" ").concat(Integer.toString(quantity));
			}
		}

		if(flag) {
			ls = requestToOtherServers(userID, null, 1, itemName, 8081, null);
			resultString += " "+ls.findItem(userID, itemName, false);
			ls = requestToOtherServers(userID, null, 1, itemName, 8082, null);
			resultString += " "+ls.findItem(userID, itemName, false);
		}
		return resultString; 
	}

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public synchronized String returnItem(String userID, String itemID) {
		log.info("Received request from " + userID +" for returning a book with book id "+itemID);
		//while(!checkBook.get(itemID));
		if(itemID.substring(0, 3).equals(ConstantValues.CONCORDIA)) {
			//checkBook.put(itemID, false);
			if(!userID.equals(userBookMapping.containsKey(userID)) && !userBookMapping.get(userID).contains(itemID)) {
				return "Borrower should return the book.";
			}
			int quantity = (Integer) dataStore.get(itemID).get(KEY_QUANTITY);
			quantity++;

			// Update Quantity
			dataStore.get(itemID).put(KEY_QUANTITY, quantity);
			userBookMapping.get(userID).remove(itemID);

			//checkBook.put(itemID, true);
			if(!(waitList.isEmpty() || waitList == null) && waitList.containsKey(itemID)) {
				String queue= waitList.get(itemID).peek();   
				if(queue != null) {
					bookAutoAssign(queue, itemID);
					return "Item assigned to waitlist user.";
				}
			} 

			return "Book Returned";
		}else{
			LibrariesServer ls = requestToOtherServers(userID, itemID, 3,null, 0, null);
			return ls.returnItem(userID, itemID);
		}
	}

	private void bookAutoAssign(String userId, String itemID){
		log.info("Book auto assigning");
		int qty = (Integer)dataStore.get(itemID).get(KEY_QUANTITY);

		while(waitList.size() != 0 && qty != 0) {
			qty--;
			dataStore.get(itemID).put(KEY_QUANTITY, qty);
			userQueue.poll();
			ArrayList<String> bookIds = new ArrayList<String>();
			bookIds.add(itemID);
			userBookMapping.put(userId, bookIds);
			if(userQueue == null || userQueue.isEmpty()) {
				break;
			}else {
				waitList.put(itemID,userQueue);
			}
		}
	}

	public String borrowedBookOrNot(String userId, String oldItemID, String newItemID) {
		return userBookMapping != null && !userBookMapping.isEmpty() && userBookMapping.containsKey(userId) &&
				userBookMapping.get(userId).contains(oldItemID) ? "true" : "false";
	}

	public String bookExistOrNot(String userID, String newItemID, String oldItemID) {  
		int quantity = 0;
		if(!userID.substring(0,3).equals(ConstantValues.CONCORDIA) && 
				(userBookMapping != null || !userBookMapping.isEmpty()) && userBookMapping.containsKey(userID)) {

			ArrayList<String> name = userBookMapping.get(userID);
			String borrowedBookId = null;
			for (String bookItemId : name) {
				if(bookItemId.contains(ConstantValues.CONCORDIA)) {
					borrowedBookId = bookItemId;
					break;
				}
			}
			if(borrowedBookId == null || (borrowedBookId != null && oldItemID.equals(borrowedBookId))) {
				return "true";
			}else {
				return "false";
			}
		}else{
			if(dataStore.containsKey(newItemID)) {
				//put semaphore here
				//while(!checkBook.get(newItemID));
				//checkBook.put(newItemID, false);
				for (String key : dataStore.keySet()) {
					quantity = (Integer)dataStore.get(key).get(KEY_QUANTITY);
					if (newItemID.equals(key) && quantity > 0) {
						return "true";
					}
				}
			}
		}
		return "false";
	}

	@Override
	public synchronized String exchangeItem(String userId, String newItemID, String oldItemID) {
		log.info("Received request from " + userId +" for exchanging "+ oldItemID+" with "+newItemID);
		String borrowedFlag = "false";
		String existFlag = "false";
		String returnBook = null;
		String bBook = null;
		LibrariesServer ls;
		if(oldItemID.substring(0, 3).equals(ConstantValues.CONCORDIA)) {
			borrowedFlag = borrowedBookOrNot(userId, oldItemID, newItemID);
		}else {
			ls = requestToOtherServers(userId, oldItemID, 6,null, 0, newItemID);
			borrowedFlag = ls.borrowedBookOrNot(userId, oldItemID, newItemID);
		}
		if(newItemID.substring(0, 3).equals(ConstantValues.CONCORDIA)) {
			existFlag = bookExistOrNot(userId, newItemID, oldItemID);
		}else {
			ls = requestToOtherServers(userId, newItemID, 7,null, 0, oldItemID);
			existFlag = ls.bookExistOrNot(userId, newItemID, oldItemID);
		}
		if("true".equals(borrowedFlag.trim()) && "true".equals(existFlag.trim())) {
			returnBook = returnItem(userId, oldItemID);
			bBook = borrowBook(userId, newItemID);
			System.out.println("Response "+returnBook+" "+ bBook);
			//checkBook.put(newItemID, true);
			return "Item exchanged successfully";
		}
		//checkBook.put(newItemID, true);
		return "Item can't excahnge";
	}

	public String borrowBook(String userID, String itemID) {
		if(itemID.substring(0, 3).equals(ConstantValues.CONCORDIA)) {
			if(!dataStore.containsKey(itemID)) return "Book doesn't exist";
			//while(!checkBook.get(itemID));
			//checkBook.put(itemID, false);

			int quantity = (Integer) dataStore.get(itemID).get(KEY_QUANTITY);
			if (quantity > 0 && dataStore.containsKey(itemID)) {
				quantity--;

				// Update Quantity
				dataStore.get(itemID).put(KEY_QUANTITY, quantity);

				// Add item to user's book id mapping
				if(userBookMapping == null || userBookMapping.isEmpty() || !userBookMapping.containsKey(userID)) {
					ArrayList<String> bookIds = new ArrayList<String>();
					bookIds.add(itemID);
					userBookMapping.put(userID, bookIds);
				}else if(userBookMapping.get(userID).contains(itemID)){
					return "You have already borrowed this book"; 
				}else if (userBookMapping.containsKey(userID)) {
					userBookMapping.get(userID).add(itemID);
				} 

				//checkBook.put(itemID, true);
				return "Book issued!";
			} else if(quantity == 0) {
				if((userBookMapping != null || !userBookMapping.isEmpty()) && userBookMapping.containsKey(userID)) {
					//checkBook.put(itemID, true);
					if(userBookMapping.get(userID).contains(itemID)) {
						return "You have already borrowed this book"; 
					}else {
						return "";
					}
				}else {
					//checkBook.put(itemID, true);
					return "Book not available!"+"\n"+
					"Do you want to lent the book once available? Press 1 for yes, 2 for no: ";
				}
			}else {
				//checkBook.put(itemID, true);
				return "Book doesn't exist";
			}
		}else {
			LibrariesServer ls = requestToOtherServers(userID, itemID, 2,null, 0, null);
			return ls.borrowItem(userID, itemID);
		}
	}

	public void updateConServerLog(Logger log) throws SecurityException, IOException {
		FileHandler fileHandler = new FileHandler(System.getProperty("user.dir")+"/Logger/"+"conserver"+".log", true);
		log.addHandler(fileHandler);
		fileHandler.setFormatter(new SimpleFormatter());
	}

	public LibrariesServer requestToOtherServers(String userId, String itemId, int serverNumber,String itemName, int serPort, String oldItemID) {
		log.info("Requesting other server from concordia ");
		URL compURL = null;
		QName compQName = null ;
		if(itemId != null) {
			try {
				switch (itemId.substring(0, 3)) {
				case ConstantValues.CONCORDIA:
					compURL = new URL("http://localhost:8080/comp?wsdl");
					compQName = new QName("http://serverInterfaceImplementation/", "ConcordiaLibraryImplementationService");
					log = Logger.getLogger(ConcordiaLibraryImplementation.class.getName());
					break;
				case ConstantValues.MCGILL:
					compURL = new URL("http://localhost:8082/comp?wsdl");
					compQName = new QName("http://serverInterfaceImplementation/", "McgillLibraryImplementationService");
					log = Logger.getLogger(McgillLibraryImplementation.class.getName());
					break;
				case ConstantValues.MONTREAL:
					compURL = new URL("http://localhost:8081/comp?wsdl");
					compQName = new QName("http://serverInterfaceImplementation/", "MontrealLibraryImplementationService");
					log = Logger.getLogger(MontrealLibraryImplementation.class.getName());
					break;
				default:
					break;
				}
			}catch(MalformedURLException ex) {
				ex.printStackTrace();
			}
		}else {
			try {
				if(serverNumber == 1 && serPort == 8082) {
					compURL = new URL("http://localhost:8082/comp?wsdl");
					compQName = new QName("http://serverInterfaceImplementation/", "McgillLibraryImplementationService");
					log = Logger.getLogger(McgillLibraryImplementation.class.getName());
				}else if(serverNumber == 1 && serPort == 8081) {
					compURL = new URL("http://localhost:8081/comp?wsdl");
					compQName = new QName("http://serverInterfaceImplementation/", "MontrealLibraryImplementationService");
					log = Logger.getLogger(MontrealLibraryImplementation.class.getName());
				}
			}catch(MalformedURLException ex) {
				ex.printStackTrace();
			}
		}


		Service compService = Service.create(compURL, compQName);
		return compService.getPort(LibrariesServer.class);

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataStore == null) ? 0 : dataStore.hashCode());
		result = prime * result + ((userBookMapping == null) ? 0 : userBookMapping.hashCode());
		result = prime * result + ((waitList == null) ? 0 : waitList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConcordiaLibraryImplementation other = (ConcordiaLibraryImplementation) obj;
		if (dataStore == null) {
			if (other.dataStore != null)
				return false;
		} else if (!dataStore.equals(other.dataStore))
			return false;
		if (userBookMapping == null) {
			if (other.userBookMapping != null)
				return false;
		} else if (!userBookMapping.equals(other.userBookMapping))
			return false;
		if (waitList == null) {
			if (other.waitList != null)
				return false;
		} else if (!waitList.equals(other.waitList))
			return false;
		return true;
	}

	public HashMap<String, HashMap<String, Object>> getBooksData() {
		return dataStore;
	}

	public static HashMap<String, ArrayList<String>> getUserBookMapping() {
		return userBookMapping;
	}
}
