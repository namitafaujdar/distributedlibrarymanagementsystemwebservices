package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import javax.xml.ws.Endpoint;

import constants.ConstantValues;
import serverInterfaceImplementation.McgillLibraryImplementation;

/**
 * 
 * @author Namita Faujdar
 *
 */

public class McgillServer {

	public static void main(String[] args) throws Exception{

		McgillLibraryImplementation mcgStub = new McgillLibraryImplementation();
		
		Endpoint endpoint = Endpoint.publish("http://localhost:8082/comp", mcgStub);
		System.out.println("McGill server started.....");
		}

}
