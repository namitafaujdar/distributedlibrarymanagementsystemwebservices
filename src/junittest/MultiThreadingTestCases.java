package junittest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import serverInterfaceImplementation.ConcordiaLibraryImplementation;
import serverInterfaceImplementation.McgillLibraryImplementation;
import serverInterfaceImplementation.MontrealLibraryImplementation;



public class MultiThreadingTestCases {
	static ConcordiaLibraryImplementation concordiaLibraryImplementation;
	static McgillLibraryImplementation mcgillLibraryImplementation;
	static MontrealLibraryImplementation montrealLibraryImplementation;
	
	@BeforeClass
	public static void beforeEachRun() {
		concordiaLibraryImplementation = new ConcordiaLibraryImplementation();
		mcgillLibraryImplementation = new McgillLibraryImplementation();
		montrealLibraryImplementation = new MontrealLibraryImplementation();
	}


	
	@Test
	public void addItemThreadTest() {
		Runnable addItemImplConc = () ->{
			String add = mcgillLibraryImplementation.addItem("MCGM1111", "MCG6231", "Distributed", 1);
			System.out.println(add);
			assertEquals(0, mcgillLibraryImplementation.getBooksData().get("MCG6231").get("Distributed"));
		};
		Thread thread1 = new Thread(addItemImplConc);
		
		Runnable addItemImplMcg = () ->{
			String borrow = mcgillLibraryImplementation.borrowItem("MCGU1111", "MCG6231");
			System.out.println(borrow);
			assertEquals(0, mcgillLibraryImplementation.getBooksData().get("MCG6231").get("Distributed"));
		};
		Thread thread2 = new Thread(addItemImplMcg);
		
		Runnable addItemImplMon = () ->{
			String remove = mcgillLibraryImplementation.removeItem("MCGM1111", "MCG6231", 1);
			System.out.println(remove);
			assertEquals(0, mcgillLibraryImplementation.getBooksData().get("MCG6231").get("Distributed"));
		};
		Thread thread3 = new Thread(addItemImplMon);
		
		thread1.start();
		thread2.start();
		thread3.start();
	}
	
//	@Test
//	public void listItemThreadTest() {
//		Runnable listItemImplConc = () ->{
//			mcgillLibraryImplementation.listItemAvailability("MCGM1111");
//		};
//		Thread thread1 = new Thread(listItemImplConc);
//		Runnable listItemImplMcg = () ->{
//			montrealLibraryImplementation.listItemAvailability("MONM1111");
//		};
//		Thread thread2 = new Thread(listItemImplMcg);
//		Runnable listItemImplMon = () ->{
//			concordiaLibraryImplementation.listItemAvailability("CONM1111");
//		};
//		Thread thread3 = new Thread(listItemImplMon);
//		thread1.start();
//		thread2.start();
//		thread3.start();
//	}
//	
//	@Test
//	public void removeItemThreadTest() {
//		Runnable removeItemImplConc = () ->{
//			mcgillLibraryImplementation.removeItem("MCGM1111", "MCG6231", 1);
//		};
//		Thread thread1 = new Thread(removeItemImplConc);
//		Runnable removeItemImplMcg = () ->{
//			montrealLibraryImplementation.removeItem("MONM1111", "MON6231", 1);
//		};
//		Thread thread2 = new Thread(removeItemImplMcg);
//		Runnable removeItemImplMon = () ->{
//			concordiaLibraryImplementation.removeItem("CONM1111", "CON6231", 1);
//		};
//		Thread thread3 = new Thread(removeItemImplMon);
//		thread1.start();
//		thread2.start();
//		thread3.start();
//	}
//	
//	@Test
//	public void borrowItemList() {
//		Runnable borrowItemImplConc = () ->{
//			concordiaLibraryImplementation.borrowItem("CONU1111", "CON6231");
//		};
//		Thread thread1 = new Thread(borrowItemImplConc);
//		Runnable borrowItemImplMcg = () ->{
//			mcgillLibraryImplementation.borrowItem("MCGU1111","MCG6231");
//		};
//		Thread thread2 = new Thread(borrowItemImplMcg);
//		Runnable borrowItemImplMon = () ->{
//			montrealLibraryImplementation.borrowItem("MONU1111", "MON6231");
//		};
//		Thread thread3 = new Thread(borrowItemImplMon);
//		thread1.start();
//		thread2.start();
//		thread3.start();
//	}
//	
//	@Test
//	public void returnItemThreadTest() {
//		Runnable returnItemImplConc = () ->{
//			concordiaLibraryImplementation.returnItem("CONU1111", "CON6231");
//		};
//		Thread thread1 = new Thread(returnItemImplConc);
//		Runnable returnItemImplMcg = () ->{
//			mcgillLibraryImplementation.returnItem("MCGU1111", "MCG6231");
//		};
//		Thread thread2 = new Thread(returnItemImplConc);
//		Runnable returnItemImplMon = () ->{
//			montrealLibraryImplementation.returnItem("MONU1111", "MON6231");
//		};
//		Thread thread3 = new Thread(returnItemImplMon);
//		thread1.start();
//		thread2.start();
//		thread3.start();
//	}
//	
//	@Test
//	public void findItemThreadTest() {
//		Runnable findItemImplConc = () ->{
//			concordiaLibraryImplementation.findItem("CONU1111", "DISTRIBUTED",false);
//		};
//		Thread thread1 = new Thread(findItemImplConc);
//		Runnable findItemImplMcg = () ->{
//			mcgillLibraryImplementation.findItem("MCGU1111","DISTRIBUTED",false);
//		};
//		Thread thread2 = new Thread(findItemImplMcg);
//		Runnable findItemImplMon = () ->{
//			montrealLibraryImplementation.findItem("MONU1111", "DISTRIBUTED",false);
//		};
//		Thread thread3 = new Thread(findItemImplMon);
//		thread1.start();
//		thread2.start();
//		thread3.start();
//	}
//
//	@Test
//	public void exchangeItemThreadTest() {
//		Runnable exchangeItemImplConc = () ->{
//			concordiaLibraryImplementation.exchangeItem("CONU1111", "CON6441",  "CON6231");
//			assertTrue(concordiaLibraryImplementation.getUserBookMapping().get("CON6441").contains("CONU1111"));
//		};
//		Thread thread1 = new Thread(exchangeItemImplConc);
//		Runnable exchangeItemImplMcg = () ->{
//			mcgillLibraryImplementation.exchangeItem("MCGU1111", "MCG6440",  "MCG6231");
//			assertTrue(mcgillLibraryImplementation.getUserBookMapping().get("MCG6231").contains("MCGU1111"));
//		};
//		Thread thread2 = new Thread(exchangeItemImplMcg);
//		Runnable exchangeItemImplMon = () ->{
//			montrealLibraryImplementation.exchangeItem("MONU1111", "MON6441",  "MON6231");
//			assertTrue(montrealLibraryImplementation.getUserBookMapping().get("MON6441").contains("MONU1111"));
//		};
//		Thread thread3 = new Thread(exchangeItemImplMon);
//		thread1.start();
//		thread2.start();
//		thread3.start();
//	}
}

