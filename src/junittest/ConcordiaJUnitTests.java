package junittest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import serverInterfaceImplementation.ConcordiaLibraryImplementation;

public class ConcordiaJUnitTests {
	ConcordiaLibraryImplementation concordiaLibraryImplementation;
	
	@Before
	public void beforeEachRun() {
		concordiaLibraryImplementation=new ConcordiaLibraryImplementation();
		concordiaLibraryImplementation.addItem("CONM1111", "CON6231", "Distributed", 1);
		concordiaLibraryImplementation.addItem("CONM1111", "CON6231", "Distributed", 5);
		concordiaLibraryImplementation.addItem("CONM1111", "CON6441", "APP", 1);
	}

	@Test
	public void addItemThreadTest() {
		Runnable addItemImplConc = () ->{
			concordiaLibraryImplementation.addItem("CONM1111", "CON6231", "Distributed", 1);
		};
		Thread thread1 = new Thread(addItemImplConc);
		Runnable addItemImplCON = () ->{
			concordiaLibraryImplementation.addItem("CONM1111", "CON6231", "Distributed", 5);
		};
		Thread thread2 = new Thread(addItemImplCON);
		Runnable addItemImplTwo = () ->{
			concordiaLibraryImplementation.addItem("CONM1111", "CON6441", "APP", 1);
		};
		Thread thread3 = new Thread(addItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(15, concordiaLibraryImplementation.getBooksData().get("CON6231").get("quantity"));
	}
	
	@Test
	public void listItemThreadTest() {
		Runnable listItemImplConc = () ->{
			concordiaLibraryImplementation.listItemAvailability("CONM1111");
		};
		Thread thread1 = new Thread(listItemImplConc);
		Runnable listItemImplCON = () ->{
			concordiaLibraryImplementation.listItemAvailability("CONM1111");
		};
		Thread thread2 = new Thread(listItemImplCON);
		Runnable listItemImplTwo = () ->{
			concordiaLibraryImplementation.listItemAvailability("CONM1111");
		};
		Thread thread3 = new Thread(listItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(21, concordiaLibraryImplementation.getBooksData().get("CON6231").get("quantity"));
	}
	
	@Test
	public void removeItemThreadTest() {
		Runnable removeItemImplConc = () ->{
			concordiaLibraryImplementation.removeItem("CONM1111", "CON6231", 1);
		};
		Thread thread1 = new Thread(removeItemImplConc);
		Runnable removeItemImplCON = () ->{
			concordiaLibraryImplementation.removeItem("CONM1111", "CON6231", 1);
		};
		Thread thread2 = new Thread(removeItemImplCON);
		Runnable removeItemImplTwo = () ->{
			concordiaLibraryImplementation.removeItem("CONM1111", "CON6231", 1);
		};
		Thread thread3 = new Thread(removeItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(30, concordiaLibraryImplementation.getBooksData().get("CON6231").get("quantity"));
	}
	
	@Test
	public void borrowItemList() {
		Runnable borrowItemImplConc = () ->{
			concordiaLibraryImplementation.borrowItem("CONU1111", "CON6231");
		};
		Thread thread1 = new Thread(borrowItemImplConc);
		Runnable borrowItemImplCON = () ->{
			concordiaLibraryImplementation.borrowItem("CONU1111","CON6231");
		};
		Thread thread2 = new Thread(borrowItemImplCON);
		Runnable borrowItemImplTwo = () ->{
			concordiaLibraryImplementation.borrowItem("CONU1111", "CON6231");
		};
		Thread thread3 = new Thread(borrowItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(3, concordiaLibraryImplementation.getBooksData().get("CON6231").get("quantity"));
	}
	
	@Test
	public void returnItemThreadTest() {
		Runnable returnItemImplConc = () ->{
			concordiaLibraryImplementation.returnItem("CONM1111", "CON6231");
		};
		Thread thread1 = new Thread(returnItemImplConc);
		Runnable returnItemImplCON = () ->{
			concordiaLibraryImplementation.returnItem("CON1111", "CON6231");
		};
		Thread thread2 = new Thread(returnItemImplConc);
		Runnable returnItemImpTwo = () ->{
			concordiaLibraryImplementation.returnItem("CONM1111", "CON6231");
		};
		Thread thread3 = new Thread(returnItemImpTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(27, concordiaLibraryImplementation.getBooksData().get("CON6231").get("quantity"));
	}
	
	@Ignore
	public void findItemThreadTest() {
		Runnable findItemImplConc = () ->{
			concordiaLibraryImplementation.findItem("CONU1111", "DISTRIBUTED",false);
		};
		Thread thread1 = new Thread(findItemImplConc);
		Runnable findItemImplCON = () ->{
			concordiaLibraryImplementation.findItem("CONU1111","DISTRIBUTED",false);
		};
		Thread thread2 = new Thread(findItemImplCON);
		Runnable findItemImplTwo = () ->{
			concordiaLibraryImplementation.findItem("CONU1111", "DISTRIBUTED",false);
		};
		Thread thread3 = new Thread(findItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
	}

	@Ignore
	public void exchangeItemThreadTest() {
		Runnable exchangeItemImplConc = () ->{
			concordiaLibraryImplementation.exchangeItem("CONM1111", "CON6441",  "CON6231");
		};
		Thread thread1 = new Thread(exchangeItemImplConc);
		Runnable exchangeItemImplCON = () ->{
			concordiaLibraryImplementation.exchangeItem("CONM1111", "CON6440",  "CON6231");
		};
		Thread thread2 = new Thread(exchangeItemImplCON);
		Runnable exchangeItemImplTwo = () ->{
			concordiaLibraryImplementation.exchangeItem("CONM1111", "CON6441",  "CON6231");
		};
		Thread thread3 = new Thread(exchangeItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, concordiaLibraryImplementation.getBooksData().get("CON6231").get("qunatity"));
	}
}

