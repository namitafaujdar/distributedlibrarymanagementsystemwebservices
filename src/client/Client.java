package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import constants.ConstantValues;
import serverInterface.LibrariesServer;
import serverInterfaceImplementation.ConcordiaLibraryImplementation;
import serverInterfaceImplementation.McgillLibraryImplementation;
import serverInterfaceImplementation.MontrealLibraryImplementation;

public class Client {

	private static Logger log;

	private static String serverSelection(String str) {
		str = str.substring(0, 3);
		if (str.equals(ConstantValues.CONCORDIA)) {
			return ConstantValues.CONCORDIA;
		} else if (str.equals(ConstantValues.MCGILL)) {
			return ConstantValues.MCGILL;
		} else if (str.equals(ConstantValues.MONTREAL)) {
			return ConstantValues.MONTREAL;
		}
		return str;
	}

	private static void allUser(String userID) {
		String itemName = null;
		String itemId = null;
		String newItemId = null;
		String oldItemId = null;
		String string;
		try {
			LibrariesServer librariesObject = createObject(userID);
			updateClientLog(log, userID);
			System.out.println("Welcome " + userID + " to Concordia Library Management System:" + "\n"
					+ "Press 1 for Finding a book." + "\n" + "Press 2 for Borrowing a book." + "\n"
					+ "Press 3 for Returning a book." + "\n"
					+ "Press 4 for Exchanging a book." + "\n");
			BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
			int number = Integer.parseInt(bfr.readLine());
			switch (number) {
			case 1:
				// ******************* FIND ITEM ****************
				System.out.println("Please enter name of the book: ");
				itemName = bfr.readLine();
				log.info("User "+userID+" finding "+itemName+" book.");
				string = librariesObject.findItem(userID, itemName,true);
				log.info("Response of server: " + string);
				break;
			case 2:
				// ******************* BORROW ITEM ****************
				System.out.println("Please enter item id: ");
				itemId = enterItemId(bfr);
				log.info("User "+userID+" borrowing "+itemId);
				string = librariesObject.borrowItem(userID, itemId);
				log.info("Response of server: " + string);
				if(string.contains("lent")) {
					int inputString = Integer.parseInt(bfr.readLine()); 
					if(inputString == 1) {
						string = librariesObject.waitingQueueList(userID, itemId); 
						log.info("Response of server: " +string);
					} else if(inputString == 2) { log.info("Thank You"); }
				}
				break;
			case 3:
				// ******************* RETURN ITEM ****************
				System.out.print("Please enter item id: ");
				itemId = enterItemId(bfr);
				log.info("User "+userID+" returning "+itemId);
				string = librariesObject.returnItem(userID, itemId);
				log.info("Response of server: " +string);
				break;
			case 4:
				// ******************* EXCHANGE ITEM ****************
				System.out.print("Please enter new item id: ");
				newItemId = enterItemId(bfr);
				System.out.print("Please enter old item id: ");
				oldItemId = enterItemId(bfr);
				log.info("User "+userID+" exchanging "+oldItemId+" with "+newItemId);
				string = librariesObject.exchangeItem(userID, newItemId, oldItemId);
				log.info("Response of server: " +string);
				break;
			default:
				log.info("Please enter a valid number.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	public static void allManager(String managerID) {

		try {
			LibrariesServer librariesObject = createObject(managerID);
			updateClientLog(log, managerID);
			System.out.println("Welcome " + managerID + " to Concordia Library Management System:" + "\n"
					+ "Press 1 to add a book." + "\n" + "Press 2 to remove  a book." + "\n"
					+ "Press 3 to list book availability." + "\n");
			BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
			int number = Integer.parseInt(bfr.readLine().trim());
			String resultString;
			switch (number) {
			case 1:
				// ******************* ADD ITEM ****************
				managerAdd(librariesObject, managerID);
				break;
			case 2:
				// ******************* REMOVE ITEM ****************
				managerRemove(librariesObject, managerID);
				break;
			case 3:
				// ******************* LIST AVAILABLE ITEM ****************
				resultString = librariesObject.listItemAvailability(managerID);
				log.info("Response of server: " +resultString);
				break;
			default:
				log.info("Please enter a valid input.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void managerRemove(LibrariesServer obj, String managerID) {
		String itemID;
		int quantity = 0;
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));

		try {
			System.out.print("Please enter item id: ");
			itemID = enterItemId(bfr);
			log.info("Manager "+managerID+" removing book with book id "+itemID);
			System.out.println();
			System.out.println("Do you want to delete the item or decrease the quantity?" + "\n"
					+ "Press 1 to delete the item." + "\n" + "Press 2 to decrease the item quantity.");
			int input = Integer.parseInt(bfr.readLine());
			if (input == 1) {
				log.info("Manager "+managerID+" selected to remove book with book id "+itemID);
				quantity = -1;
			} else if (input == 2) {
				System.out.print("Please enter quantity: ");
				quantity = Integer.parseInt(bfr.readLine());
				log.info("Manager "+managerID+" selected to decrease the quantity of book "+itemID+" by "+quantity);
			}
			String string = obj.removeItem(managerID, itemID, quantity);
			log.info("Response of server: " +string);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void managerAdd(LibrariesServer obj, String managerID) {
		String itemID;
		String itemName;
		int quantity = 0;
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));

		try {
			System.out.print("Please enter item id: ");
			itemID = enterItemId(bfr);
			System.out.println();
			System.out.print("Please enter name of the book: ");
			itemName = bfr.readLine();
			System.out.println();
			System.out.print("Please enter quantity: ");
			quantity = Integer.parseInt(bfr.readLine());
			log.info("Manager "+managerID+" adding a new book with book id "+itemID+" ,book name "+itemName+" and quantity "+quantity);
			String string = obj.addItem(managerID, itemID, itemName, quantity);
			log.info("Response of server: " +string);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String enterItemId(BufferedReader bfr) { 
		String s;
		try {
			s = bfr.readLine();
			if((s.substring(0, 3).equals(ConstantValues.CONCORDIA) ||
					s.substring(0, 3).equals(ConstantValues.MONTREAL) ||
					s.substring(0, 3).equals(ConstantValues.MCGILL))
					&& s.substring(3).matches("[0-9]+") && s.length() == 7) {
				return s;
			}else {
				System.out.println("Please enter valid Item id: ");
				return enterItemId(bfr);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Invalid item id";
	} 

	private static void updateClientLog(Logger log, String userId) throws SecurityException, IOException {
		FileHandler fileHandler = new FileHandler(System.getProperty("user.dir")+"/Logger/"+userId+".log", true);
		log.addHandler(fileHandler);
		fileHandler.setFormatter(new SimpleFormatter());
	}

	public static LibrariesServer createObject(String userID) {
		URL compURL = null;
		QName compQName = null ;
		try {
			switch (userID.substring(0, 3)) {
			case ConstantValues.CONCORDIA:
				compURL = new URL("http://localhost:8080/comp?wsdl");
				compQName = new QName("http://serverInterfaceImplementation/", "ConcordiaLibraryImplementationService");
				log = Logger.getLogger(ConcordiaLibraryImplementation.class.getName());
				break;
			case ConstantValues.MCGILL:
				compURL = new URL("http://localhost:8082/comp?wsdl");
				compQName = new QName("http://serverInterfaceImplementation/", "McgillLibraryImplementationService");
				log = Logger.getLogger(McgillLibraryImplementation.class.getName());
				break;
			case ConstantValues.MONTREAL:
				compURL = new URL("http://localhost:8081/comp?wsdl");
				compQName = new QName("http://serverInterfaceImplementation/", "MontrealLibraryImplementationService");
				log = Logger.getLogger(MontrealLibraryImplementation.class.getName());
				break;
			default:
				break;
			}
		}catch(MalformedURLException ex) {
			ex.printStackTrace();
		}

		Service compService = Service.create(compURL, compQName);
		return compService.getPort(LibrariesServer.class);
	}

	public static void main(String[] args) {
		try {
			BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter your ID ");
			String id = bfr.readLine();

			String userValue = null;
			String managerValue = null;

			// ************************ CHECKING WHETHER IT'S A USER OR MANAGER *********************
			if (id.charAt(3) == 'U' && id.length() == 8) {
				userValue = serverSelection(id);
			} else if (id.charAt(3) == 'M' && id.length() == 8) {
				managerValue = serverSelection(id);
			} else {
				System.out.println("Invalid Id");
			}


			// ***************** USER CHECK **************************
			if (userValue != null) {
				allUser(id);
			}

			// ***************** MANAGER CHECK ***********************
			if (managerValue != null) {
				allManager(id);
			}

		} catch (Exception e) {
			System.out.println("Hello Client exception: " + e);
			e.printStackTrace();
		}
	}
}
