package junittest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ConcordiaJUnitTests.class, McgillTests.class, MontrealImplementationTests.class,
		MultiThreadingTestCases.class })
public class AllTests {

}
