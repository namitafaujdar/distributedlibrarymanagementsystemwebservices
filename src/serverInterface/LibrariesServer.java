package serverInterface;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * 
 * @author Namita Faujdar
 *
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface LibrariesServer {

	String addItem(String managerID, String itemID, String itemName, int quantity);
	String removeItem(String managerID, String itemID, int quantity);
	String listItemAvailability(String managerID);

	String borrowItem(String userID, String itemID);
	String findItem(String userID, String itemName, boolean flag);
	String returnItem(String userID, String itemID);
	String waitingQueueList(String userID, String itemID);
	String exchangeItem(String userId, String newItemID, String oldItemID);
	String borrowedBookOrNot(String userId, String oldItemID, String newItemID);
	String bookExistOrNot(String userId, String newItemID, String oldItemID);
}
