package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import javax.xml.ws.Endpoint;

import constants.ConstantValues;
import serverInterfaceImplementation.MontrealLibraryImplementation;

/**
 * 
 * @author Namita Faujdar
 *
 */

public class MontrealServer {

	public static void main(String[] args) throws Exception{

		MontrealLibraryImplementation monStub = new MontrealLibraryImplementation();
		
		Endpoint endpoint = Endpoint.publish("http://localhost:8081/comp", monStub);
		System.out.println("Montreal server started.....");
		}

}